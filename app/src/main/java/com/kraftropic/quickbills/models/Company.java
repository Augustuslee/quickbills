package com.kraftropic.quickbills.models;

import android.os.Parcel;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;

import com.kraftropic.quickbills.R;

import java.util.Date;

@Entity(tableName = "companies")
public class Company extends QuickModel {
    @ColumnInfo(name = "name")
    private String name;
    @ColumnInfo(name = "alias")
    private String alias;
    @ColumnInfo(name = "phone")
    private String phone;
    @ColumnInfo(name = "email")
    private String email;
    @ColumnInfo(name = "address")
    private String address;
    @ColumnInfo(name = "tag_line")
    private String tagLine;
    @ColumnInfo(name = "motto")
    private String motto;
    @ColumnInfo(name = "vision")
    private String vision;
    @ColumnInfo(name = "mission")
    private String mission;
    @ColumnInfo(name = "theme")
    private Integer theme;
    @ColumnInfo(name = "stars")
    private Integer stars;

    public Company() {
        this.name = "";
        this.alias = "";
        this.phone = "";
        this.email = "";
        this.address = "";
        this.tagLine = "";
        this.motto = "";
        this.vision = "";
        this.mission = "";
        this.theme = R.layout.model_card_default;
        this.stars = 0;
    }

    @Ignore
    public Company(String name, String alias, String phone, String email, String address, String tagLine, String motto, String vision, String mission, Integer theme, Integer stars) {
        this.name = name;
        this.alias = alias;
        this.phone = phone;
        this.email = email;
        this.address = address;
        this.tagLine = tagLine;
        this.motto = motto;
        this.vision = vision;
        this.mission = mission;
        this.theme = theme;
        this.stars = stars;
    }

    @Ignore
    public Company(String uid, String name, String alias, String phone, String email, String address, String tagLine, String motto, String vision, String mission, Integer theme, Integer stars) {
        super(uid);
        this.name = name;
        this.alias = alias;
        this.phone = phone;
        this.email = email;
        this.address = address;
        this.tagLine = tagLine;
        this.motto = motto;
        this.vision = vision;
        this.mission = mission;
        this.theme = theme;
        this.stars = stars;
    }

    @Ignore
    public Company(Date dateCreated, Date dateModified, String uid, String name, String alias, String phone, String email, String address, String tagLine, String motto, String vision, String mission, Integer theme, Integer stars) {
        super(dateCreated, dateModified, uid);
        this.name = name;
        this.alias = alias;
        this.phone = phone;
        this.email = email;
        this.address = address;
        this.tagLine = tagLine;
        this.motto = motto;
        this.vision = vision;
        this.mission = mission;
        this.theme = theme;
        this.stars = stars;
    }

    @Ignore
    public Company(Parcel in, String name, String alias, String phone, String email, String address, String tagLine, String motto, String vision, String mission, Integer theme, Integer stars) {
        super(in);
        this.name = name;
        this.alias = alias;
        this.phone = phone;
        this.email = email;
        this.address = address;
        this.tagLine = tagLine;
        this.motto = motto;
        this.vision = vision;
        this.mission = mission;
        this.theme = theme;
        this.stars = stars;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTagLine() {
        return tagLine;
    }

    public void setTagLine(String tagLine) {
        this.tagLine = tagLine;
    }

    public String getMotto() {
        return motto;
    }

    public void setMotto(String motto) {
        this.motto = motto;
    }

    public String getVision() {
        return vision;
    }

    public void setVision(String vision) {
        this.vision = vision;
    }

    public String getMission() {
        return mission;
    }

    public void setMission(String mission) {
        this.mission = mission;
    }

    public Integer getTheme() {
        return theme;
    }

    public void setTheme(Integer theme) {
        this.theme = theme;
    }

    public Integer getStars() {
        return stars;
    }

    public void setStars(Integer stars) {
        this.stars = stars;
    }
}
