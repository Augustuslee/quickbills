package com.kraftropic.quickbills.models;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.PrimaryKey;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

public class QuickModel implements Parcelable {

    @ColumnInfo(name = "date_created")
    private Date dateCreated;
    @ColumnInfo(name = "date_modified")
    private Date dateModified;
    @ColumnInfo(name = "uid")
    @NonNull
    @PrimaryKey()
    private String uid;

    public QuickModel() {
        this.dateCreated = Calendar.getInstance().getTime();
        this.dateModified = Calendar.getInstance().getTime();
        this.uid = UUID.randomUUID().toString();
    }

    public QuickModel(String uid) {
        this.uid = uid;
        this.dateCreated = Calendar.getInstance().getTime();
        this.dateModified = Calendar.getInstance().getTime();
    }

    public QuickModel(Date dateCreated, Date dateModified, String uid) {
        this.dateCreated = dateCreated;
        this.dateModified = dateModified;
        this.uid = uid;
    }

    protected QuickModel(Parcel in) {
        uid = in.readString();
    }

    public static final Creator<QuickModel> CREATOR = new Creator<QuickModel>() {
        @Override
        public QuickModel createFromParcel(Parcel in) {
            return new QuickModel(in);
        }

        @Override
        public QuickModel[] newArray(int size) {
            return new QuickModel[size];
        }
    };

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateModified() {
        return dateModified;
    }

    public void setDateModified(Date dateModified) {
        this.dateModified = dateModified;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(uid);
    }
}
