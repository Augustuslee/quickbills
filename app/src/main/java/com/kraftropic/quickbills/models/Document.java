package com.kraftropic.quickbills.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;

@Entity(tableName = "documents")
public class Document extends QuickModel {
    @ColumnInfo(name = "title")
    private String title;
    @ColumnInfo(name = "summary")
    private String summary;
    @ColumnInfo(name = "type")
    private String type;
    @ColumnInfo(name = "childDocumentId")
    private String childDocumentId;

    public Document() {
        super("");
        this.title = "";
        this.summary = "";
        this.type = "";
        this.childDocumentId = "";
    }

    @Ignore
    public Document(String uid) {
        super(uid);
        this.title = "";
        this.summary = "";
        this.type = "";
        this.childDocumentId = "";
    }

    @Ignore
    public Document(String uid, String title, String summary, String type, String childDocumentId) {
        super(uid);
        this.title = title;
        this.summary = summary;
        this.type = type;
        this.childDocumentId = childDocumentId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getChildDocumentId() {
        return childDocumentId;
    }

    public void setChildDocumentId(String childDocumentId) {
        this.childDocumentId = childDocumentId;
    }
}
