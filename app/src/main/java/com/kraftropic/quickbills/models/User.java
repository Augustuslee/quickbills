package com.kraftropic.quickbills.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;

import java.util.Calendar;
import java.util.Date;

@Entity(tableName = "users")
public class User extends QuickModel {
    @ColumnInfo(name = "name")
    private String name;
    @ColumnInfo(name = "gender")
    private String gender;
    @ColumnInfo(name = "email")
    private String email;
    @ColumnInfo(name = "dob")
    private Date dob;
    @ColumnInfo(name = "location")
    private String location;

    public User() {
        this.name = "";
        this.gender = "";
        this.email = "";
        this.dob = Calendar.getInstance().getTime();
        this.location = "";
    }

    @Ignore
    public User(String uid) {
        super(uid);
        this.name = "";
        this.gender = "";
        this.email = "";
        this.dob = Calendar.getInstance().getTime();
        this.location = "";
    }

    @Ignore
    public User(String uid, String name, String gender, String email, Date dob, String location) {
        super(uid);
        this.name = name;
        this.gender = gender;
        this.email = email;
        this.dob = dob;
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
