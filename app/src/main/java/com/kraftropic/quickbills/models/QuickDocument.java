package com.kraftropic.quickbills.models;

import android.content.Intent;
import android.os.Parcel;

import java.util.Date;

public class QuickDocument extends QuickModel {
    private String title;
    private Intent intent;
    private int drawable;
    private String summary;

    public QuickDocument(String title, Intent intent, int drawable, String summary) {
        this.title = title;
        this.intent = intent;
        this.drawable = drawable;
        this.summary = summary;
    }

    public QuickDocument(String uid, String title, Intent intent, int drawable, String summary) {
        super(uid);
        this.title = title;
        this.intent = intent;
        this.drawable = drawable;
        this.summary = summary;
    }

    public QuickDocument(Date dateCreated, Date dateModified, String uid, String title, Intent intent, int drawable, String summary) {
        super(dateCreated, dateModified, uid);
        this.title = title;
        this.intent = intent;
        this.drawable = drawable;
        this.summary = summary;
    }

    public QuickDocument(Parcel in, String title, Intent intent, int drawable, String summary) {
        super(in);
        this.title = title;
        this.intent = intent;
        this.drawable = drawable;
        this.summary = summary;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Intent getIntent() {
        return intent;
    }

    public void setIntent(Intent intent) {
        this.intent = intent;
    }

    public int getDrawable() {
        return drawable;
    }

    public void setDrawable(int drawable) {
        this.drawable = drawable;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }
}
