package com.kraftropic.quickbills.fragments.bottom_sheets;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.kraftropic.quickbills.R;
import com.kraftropic.quickbills.activities.creators.CreateCompanyActivity;
import com.kraftropic.quickbills.activities.creators.CreateContactActivity;
import com.kraftropic.quickbills.activities.creators.CreateInvoiceActivity;
import com.kraftropic.quickbills.activities.creators.CreateProductActivity;
import com.kraftropic.quickbills.activities.creators.CreateReceiptActivity;
import com.kraftropic.quickbills.models.QuickDocument;
import com.kraftropic.quickbills.quick_core.custom.QuickBottomSheetFragment;

import java.util.ArrayList;
import java.util.List;

public class NewDocFragment extends QuickBottomSheetFragment {

    public static NewDocFragment newInstance() {
        return new NewDocFragment();
    }

    @Override
    public int setLayout() {
        return R.layout.fragment_new_doc;
    }

    @Override
    public void quickResume() {
        ((RecyclerView) findViewById(R.id.newDocRecycler)).setAdapter(new RecyclerView.Adapter<QuickDocViewHolder>() {
            @NonNull
            @Override
            public QuickDocViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                return new QuickDocViewHolder(LayoutInflater.from(getContext()).inflate(R.layout.model_new_quick_doc, parent, false));
            }

            @Override
            public void onBindViewHolder(@NonNull QuickDocViewHolder h, int position) {
                QuickDocument doc = getQuickDocuments().get(position);

                Glide.with(NewDocFragment.this)
                        .load(doc.getDrawable())
                        .into(h.icon);
                h.title.setText(doc.getTitle());
                h.summary.setText(doc.getSummary());
                h.parent.setOnClickListener(v -> {
                    startActivity(doc.getIntent());
                    NewDocFragment.this.dismiss();
                });
            }

            @Override
            public int getItemCount() {
                return getQuickDocuments().size();
            }
        });

        setOnClickListenerById(R.id.back);
    }

    @Override
    public void onClick(View v) {
        dismiss();
    }

    private List<QuickDocument> getQuickDocuments() {
        List<QuickDocument> quickDocuments = new ArrayList<>();

        quickDocuments.add(new QuickDocument("Contact", new Intent(getContext(), CreateContactActivity.class), R.drawable.ic_add_user, "Create a new Business Contact"));
        quickDocuments.add(new QuickDocument("Business", new Intent(getContext(), CreateCompanyActivity.class), R.drawable.ic_business, "Create a new Business"));
        quickDocuments.add(new QuickDocument("Invoice", new Intent(getContext(), CreateInvoiceActivity.class), R.drawable.ic_doc, "Create a new Invoice"));
        quickDocuments.add(new QuickDocument("Receipt", new Intent(getContext(), CreateReceiptActivity.class), R.drawable.ic_doc, "Create a new Receipt"));
        quickDocuments.add(new QuickDocument("Product/Service", new Intent(getContext(), CreateProductActivity.class), R.drawable.ic_product, "Create a new Product or Service"));

        return quickDocuments;
    }

    public class QuickDocViewHolder extends RecyclerView.ViewHolder {

        ImageView icon;
        TextView title, summary;
        View parent;

        QuickDocViewHolder(@NonNull View itemView) {
            super(itemView);

            icon = itemView.findViewById(R.id.icon);
            title = itemView.findViewById(R.id.title);
            summary = itemView.findViewById(R.id.summary);
            parent = itemView;
        }
    }
}
