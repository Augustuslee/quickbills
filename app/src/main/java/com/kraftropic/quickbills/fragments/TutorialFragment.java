package com.kraftropic.quickbills.fragments;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.kraftropic.quickbills.R;
import com.kraftropic.quickbills.quick_core.custom.QuickFragment;
import com.kraftropic.quickbills.utils.Quick;

public class TutorialFragment extends QuickFragment {

    public static TutorialFragment getInstance(String title, String summary, int drawable) {
        Bundle bundle = new Bundle();

        bundle.putString(Quick.title, title);
        bundle.putString(Quick.summary, summary);
        bundle.putInt(Quick.drawable, drawable);

        TutorialFragment fragment = new TutorialFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int setLayout() {
        return R.layout.fragment_tutorial;
    }

    @Override
    public void quickResume() {
        Glide.with(this).load(getArguments().getInt(Quick.drawable)).into((ImageView) findViewById(R.id.tutorialBackground));
        ((TextView) findViewById(R.id.tutorialTitle)).setText(getArguments().getString(Quick.title));
        ((TextView) findViewById(R.id.tutorialSummary)).setText(getArguments().getString(Quick.summary));
    }
}
