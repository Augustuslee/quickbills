package com.kraftropic.quickbills.fragments.bottom_sheets;

import android.view.View;
import android.widget.Toast;

import com.kraftropic.quickbills.R;
import com.kraftropic.quickbills.quick_core.custom.QuickBottomSheetFragment;
import com.kraftropic.quickbills.utils.views.QuickEditText;

public class RequestResetLinkFragment extends QuickBottomSheetFragment {

    private QuickEditText editText;

    public static RequestResetLinkFragment newInstance() {
        return new RequestResetLinkFragment();
    }

    @Override
    public int setLayout() {
        return R.layout.fragment_request_reset_link;
    }

    @Override
    public void quickResume() {
        setOnClickListenerById(R.id.resetButton);
        setOnClickListenerById(R.id.back);

        editText = findViewById(R.id.resetEmailEdt);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.back)
            dismiss();
        else {
            if (editText.isNonNull() && editText.isAnEmail())
                getApp().getRepo().requestPasswordResetLink(editText.getString(), (positive, s) -> {

                    Toast.makeText(getContext(), positive ? "A reset link is on it's way" : "Something went wrong, please try again", Toast.LENGTH_SHORT).show();

                    if (positive) {
                        dismiss();
                    }
                });
        }
    }
}
