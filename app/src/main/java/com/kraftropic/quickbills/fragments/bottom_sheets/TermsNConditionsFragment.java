package com.kraftropic.quickbills.fragments.bottom_sheets;

import android.content.Intent;
import android.net.Uri;
import android.view.View;

import com.kraftropic.quickbills.R;
import com.kraftropic.quickbills.quick_core.custom.QuickBottomSheetFragment;
import com.kraftropic.quickbills.utils.interfaces.BooleanListener;

public class TermsNConditionsFragment extends QuickBottomSheetFragment {

    private BooleanListener listener;

    public static TermsNConditionsFragment newInstance(BooleanListener listener) {
        TermsNConditionsFragment fragment = new TermsNConditionsFragment();

        fragment.setListener(listener);

        return fragment;
    }

    private void setListener(BooleanListener listener) {
        this.listener = listener;
    }

    @Override
    public int setLayout() {
        return R.layout.fragment_terms_and_conditions;
    }

    @Override
    public void quickResume() {
        setOnClickListenerById(R.id.tcs);
        setOnClickListenerById(R.id.privacyPolicy);
        setOnClickListenerById(R.id.accept);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tcs:
            case R.id.privacyPolicy:
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(getResources().getString(R.string.tcs)));
                startActivity(intent);
                break;
            case R.id.accept:
                listener.onBooleanEvent(true);
                dismiss();
                break;
        }
    }
}
