package com.kraftropic.quickbills.fragments.bottom_sheets;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.kraftropic.quickbills.R;
import com.kraftropic.quickbills.activities.SignInActivity;
import com.kraftropic.quickbills.quick_core.custom.QuickBottomSheetFragment;

public class SignInPromptFragment extends QuickBottomSheetFragment {

    public static SignInPromptFragment newInstance(String title, String summary) {
        Bundle bundle = new Bundle();

        bundle.putString("title", title);
        bundle.putString("summary", summary);

        SignInPromptFragment fragment = new SignInPromptFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public int setLayout() {
        return R.layout.fragment_sign_in;
    }

    @Override
    public void quickResume() {
        setOnClickListenerById(R.id.back);
        setOnClickListenerById(R.id.dismiss);
        setOnClickListenerById(R.id.signIn);

        assert getArguments() != null;
        setTextById(R.id.title, getArguments().getString("title", ""));
        setTextById(R.id.summary, getArguments().getString("summary", ""));
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.back || v.getId() == R.id.dismiss) {
            getQuickActivity().showAdThenQuit();
            dismiss();
        } else {
            startActivityAndFinish(new Intent(getContext(), SignInActivity.class));
        }
    }
}
