package com.kraftropic.quickbills.adapters.view_holders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.kraftropic.quickbills.R;
import com.kraftropic.quickbills.quick_core.custom.QuickViewHolder;

public class CompanyViewHolder extends QuickViewHolder {

    public ImageView logo;
    public TextView name, alias, email, phone, address;

    public CompanyViewHolder(@NonNull View itemView) {
        super(itemView);

        logo = findViewById(R.id.companyLogo);
        name = findViewById(R.id.companyOfficerName);
        alias = findViewById(R.id.companyTitle);
        email = findViewById(R.id.companyName);
        phone = findViewById(R.id.companyTagLine);
        address = findViewById(R.id.companyAddress);
    }
}
