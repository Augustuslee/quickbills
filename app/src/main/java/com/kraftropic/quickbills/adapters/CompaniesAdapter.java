package com.kraftropic.quickbills.adapters;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;

import androidx.annotation.NonNull;

import com.kraftropic.quickbills.R;
import com.kraftropic.quickbills.adapters.view_holders.CompanyViewHolder;
import com.kraftropic.quickbills.models.Company;
import com.kraftropic.quickbills.quick_core.custom.QuickRecyclerAdapter;

import java.util.List;

public class CompaniesAdapter extends QuickRecyclerAdapter<Company, CompanyViewHolder> {

    public CompaniesAdapter(List<Company> companies, View emptyView) {
        super(companies, emptyView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onQuickBindViewHolder(CompanyViewHolder holder, Company company) {
        holder.name.setText(company.getName());
        holder.email.setText(company.getPhone() + "\n" + company.getEmail());
        holder.address.setText(company.getAddress());
    }

    @Override
    public Filter getFilter() {
        return null;
    }

    @NonNull
    @Override
    public CompanyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CompanyViewHolder(LayoutInflater.from(getContext()).inflate(R.layout.model_card_default, parent, false));
    }
}
