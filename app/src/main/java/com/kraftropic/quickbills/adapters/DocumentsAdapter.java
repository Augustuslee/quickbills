package com.kraftropic.quickbills.adapters;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;

import androidx.annotation.NonNull;

import com.kraftropic.kraftytime.core.KraftyTime;
import com.kraftropic.kraftytime.utils.KRAFTY;
import com.kraftropic.quickbills.R;
import com.kraftropic.quickbills.activities.viewers.ViewBusinessCardActivity;
import com.kraftropic.quickbills.activities.viewers.ViewCompanyActivity;
import com.kraftropic.quickbills.activities.viewers.ViewContactActivity;
import com.kraftropic.quickbills.activities.viewers.ViewInvoiceActivity;
import com.kraftropic.quickbills.activities.viewers.ViewLetterHeadActivity;
import com.kraftropic.quickbills.activities.viewers.ViewReceiptActivity;
import com.kraftropic.quickbills.activities.viewers.ViewUserActivity;
import com.kraftropic.quickbills.adapters.view_holders.DocViewHolder;
import com.kraftropic.quickbills.models.Document;
import com.kraftropic.quickbills.quick_core.custom.QuickRecyclerAdapter;

import java.util.List;

import static com.kraftropic.quickbills.utils.Quick.businessCards;
import static com.kraftropic.quickbills.utils.Quick.companies;
import static com.kraftropic.quickbills.utils.Quick.contacts;
import static com.kraftropic.quickbills.utils.Quick.invoices;
import static com.kraftropic.quickbills.utils.Quick.letterheads;
import static com.kraftropic.quickbills.utils.Quick.receipts;
import static com.kraftropic.quickbills.utils.Quick.uid;
import static com.kraftropic.quickbills.utils.Quick.users;

public class DocumentsAdapter extends QuickRecyclerAdapter<Document, DocViewHolder> {

    private Intent intent;

    public DocumentsAdapter(List<Document> companies, View emptyView) {
        super(companies, emptyView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onQuickBindViewHolder(DocViewHolder holder, Document company) {
        holder.title.setText(company.getTitle());
        holder.summary.setText(company.getSummary());
        KraftyTime.with(company.getDateCreated())
                .setClockMode(KRAFTY.MODE_12_HOUR)
                .setDateMode(KRAFTY.DDMMYYYY)
                .setDateText(holder.time);

        holder.parent.setOnClickListener(v -> {
            switch (company.getType()) {
                case invoices:
                    intent = new Intent(getContext(), ViewInvoiceActivity.class);
                    break;
                case users:
                    intent = new Intent(getContext(), ViewUserActivity.class);
                    break;
                case companies:
                    intent = new Intent(getContext(), ViewCompanyActivity.class);
                    break;
                case businessCards:
                    intent = new Intent(getContext(), ViewBusinessCardActivity.class);
                    break;
                case contacts:
                    intent = new Intent(getContext(), ViewContactActivity.class);
                    break;
                case receipts:
                    intent = new Intent(getContext(), ViewReceiptActivity.class);
                    break;
                case letterheads:
                    intent = new Intent(getContext(), ViewLetterHeadActivity.class);
                    break;
            }

            intent.putExtra(uid, company.getChildDocumentId());
            getContext().startActivity(intent);
        });
    }

    @Override
    public Filter getFilter() {
        return null;
    }

    @NonNull
    @Override
    public DocViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new DocViewHolder(LayoutInflater.from(getContext()).inflate(R.layout.model_doc, parent, false));
    }
}
