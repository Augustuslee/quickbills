package com.kraftropic.quickbills.adapters.view_holders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.kraftropic.quickbills.R;
import com.kraftropic.quickbills.quick_core.custom.QuickViewHolder;

public class DocViewHolder extends QuickViewHolder {

    public ImageView logo;
    public TextView title, summary, time, logoReplaceText;

    public DocViewHolder(@NonNull View itemView) {
        super(itemView);

        logo = findViewById(R.id.docLogo);
        title = findViewById(R.id.docTitle);
        summary = findViewById(R.id.docSummary);
        time = findViewById(R.id.docTime);
        logoReplaceText = findViewById(R.id.docLogoText);
    }
}
