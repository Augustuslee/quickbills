package com.kraftropic.quickbills.activities.viewers;

import com.kraftropic.quickbills.R;
import com.kraftropic.quickbills.quick_core.custom.QuickActivity;

public class ViewInvoiceActivity extends QuickActivity {

    @Override
    public int setLayout() {
        return R.layout.activity_view_invoice;
    }

    @Override
    public void quickResume() {
        activateOnBackPressed();

    }
}
