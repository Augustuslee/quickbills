package com.kraftropic.quickbills.activities;

import android.content.Intent;

import com.kraftropic.quickbills.R;
import com.kraftropic.quickbills.quick_core.custom.QuickActivity;

import static com.kraftropic.quickbills.utils.Quick.tutorial;

public class SplashActivity extends QuickActivity {

    @Override
    public int setLayout() {
        return (R.layout.activity_splash);
    }

    @Override
    public void quickResume() {
        startAndFinishActivity(getApp().getSharedPreferences().contains(tutorial) ? new Intent(getContext(), BaseActivity.class) : new Intent(getContext(), TutorialActivity.class));
    }
}
