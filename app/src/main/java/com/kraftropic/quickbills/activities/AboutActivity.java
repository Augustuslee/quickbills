package com.kraftropic.quickbills.activities;

import com.kraftropic.quickbills.R;
import com.kraftropic.quickbills.quick_core.custom.QuickActivity;

public class AboutActivity extends QuickActivity {

    @Override
    public int setLayout() {
        return R.layout.activity_about;
    }

    @Override
    public void quickResume() {
        activateOnBackPressed();

    }
}
