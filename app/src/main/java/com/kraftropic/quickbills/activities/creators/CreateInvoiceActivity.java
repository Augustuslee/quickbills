package com.kraftropic.quickbills.activities.creators;

import com.kraftropic.quickbills.R;
import com.kraftropic.quickbills.quick_core.custom.QuickActivity;

public class CreateInvoiceActivity extends QuickActivity {

    @Override
    public int setLayout() {
        return R.layout.activity_create_invoice;
    }

    @Override
    public void quickResume() {
        activateOnBackPressed();

    }
}
