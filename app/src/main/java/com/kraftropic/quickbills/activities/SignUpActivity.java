package com.kraftropic.quickbills.activities;

import android.content.Intent;
import android.view.View;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.kraftropic.quickbills.R;
import com.kraftropic.quickbills.models.User;
import com.kraftropic.quickbills.quick_core.custom.QuickActivity;
import com.kraftropic.quickbills.utils.views.QuickEditText;

import java.util.Calendar;

import static com.kraftropic.quickbills.utils.Quick.users;

public class SignUpActivity extends QuickActivity {

    QuickEditText email, password, confirmPassword, names;

    @Override
    public int setLayout() {
        return R.layout.activity_create_sign_up;
    }

    @Override
    public void quickResume() {
        setOnClickListenerById(R.id.signIn);
        setOnClickListenerById(R.id.signUp);
        setOnClickListenerById(R.id.signInLater);
        setOnClickListenerById(R.id.signUpWithGoogle);
        setOnClickListenerById(R.id.signInLater);

        names = findViewById(R.id.signInNames);
        email = findViewById(R.id.signInEmail);
        password = findViewById(R.id.signUpPassword);
        confirmPassword = findViewById(R.id.signUpConfirmPassword);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.signUp:
                if (email.isNonNull() && names.isNonNull() && password.isNonNull() && confirmPassword.isNonNull() && confirmPassword.passwordsMatch(password.getString())) {
                    getApp().getRepo().signUpWithEmailAndPassword(email.getString(), password.getString(), (positive, s) -> {
                        Toast.makeText(SignUpActivity.this, positive ? "Success, Setting up your account" : "Credentials don't match, Try Again", Toast.LENGTH_SHORT).show();
                        createProfile(getUser());
                    });
                }
                break;
            case R.id.signIn:
                startAndFinishActivity(new Intent(getContext(), SignInActivity.class));
                break;
            case R.id.signInLater:
                startAndFinishActivity(new Intent(getContext(), BaseActivity.class));
                break;
            case R.id.signUpWithGoogle:
                signUpWithGoogle();
                break;
        }
    }

    public void createProfile(User user) {
        getApp().getRepo().uploadObjects(user, user.getUid(), users, (positive, s) -> {
            Toast.makeText(SignUpActivity.this, positive ? "Success, Welcome" : "Something went wrong, Try Again", Toast.LENGTH_SHORT).show();
            if (positive)
                startAndFinishActivity(new Intent(getContext(), BaseActivity.class));
        });
    }

    public User getUser() {
        return new User(
                FirebaseAuth.getInstance().getUid(),
                names.getString(),
                "",
                email.getString(),
                Calendar.getInstance().getTime(),
                ""
        );
    }

    @Override
    public void onSuccessfulGoogleSignIn(User user) {
        createProfile(user);
    }
}
