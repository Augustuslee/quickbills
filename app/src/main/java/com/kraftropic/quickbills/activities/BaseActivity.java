package com.kraftropic.quickbills.activities;

import android.content.Intent;
import android.view.View;
import android.widget.PopupMenu;

import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.kraftropic.quickbills.R;
import com.kraftropic.quickbills.activities.creators.CreateCompanyActivity;
import com.kraftropic.quickbills.adapters.CompaniesAdapter;
import com.kraftropic.quickbills.adapters.DocumentsAdapter;
import com.kraftropic.quickbills.fragments.bottom_sheets.NewDocFragment;
import com.kraftropic.quickbills.models.Company;
import com.kraftropic.quickbills.models.Document;
import com.kraftropic.quickbills.quick_core.custom.QuickActivity;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class BaseActivity extends QuickActivity {

    CompaniesAdapter companiesAdapter;
    DocumentsAdapter documentsAdapter;

    CompositeDisposable disposable = new CompositeDisposable();
    Consumer companyConsumer, docsConsumer;

    @Override
    public int setLayout() {
        return R.layout.activity_base;
    }

    @SuppressWarnings("unChecked")
    @Override
    public void quickResume() {
        setOnClickListenerById(R.id.baseAllRecentDocs);
        setOnClickListenerById(R.id.baseMore);
        setOnClickListenerById(R.id.baseMoreRecentDocs);
        setOnClickListenerById(R.id.baseCreateCompany);
        setOnClickListenerById(R.id.baseCreateDoc);

        RecyclerView recyclerView = findViewById(R.id.baseCompaniesRecycler);
        RecyclerView docsRecyclerView = findViewById(R.id.baseRecentDocsRecycler);

        companyConsumer = (Consumer<List<Company>>) companies -> {
            companiesAdapter = new CompaniesAdapter(companies, findViewById(R.id.baseCompaniesRecyclerEmpty));
            recyclerView.setAdapter(companiesAdapter);
        };

        docsConsumer = (Consumer<List<Document>>) companies -> {
            documentsAdapter = new DocumentsAdapter(companies, findViewById(R.id.baseDocsRecyclerEmpty));
            docsRecyclerView.setAdapter(companiesAdapter);
        };

        Disposable docDisposable = getApp().getRepo().getDocuments().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(docsConsumer);

        Disposable disposable = getApp().getRepo().getCompanies().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(companyConsumer);

        this.disposable.add(disposable);
        this.disposable.add(docDisposable);
    }

    @Override
    protected void onStop() {
        this.disposable.dispose();
        super.onStop();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.baseMoreRecentDocs:
            case R.id.baseAllRecentDocs:
                break;
            case R.id.baseMore:
                PopupMenu menu = new PopupMenu(getContext(), v);
                menu.inflate(R.menu.base_menu);
                menu.getMenu().getItem(2).setVisible(!getApp().getRepo().loggedIn());
                menu.getMenu().getItem(3).setVisible(getApp().getRepo().loggedIn());
                menu.setOnMenuItemClickListener(item -> {
                    switch (item.getItemId()) {
                        case R.id.signIn:
                            startActivity(new Intent(getContext(), SignInActivity.class));
                            break;
                        case R.id.signOut:
                            FirebaseAuth.getInstance().signOut();
                            startAndFinishActivity(new Intent(getContext(), SplashActivity.class));
                            break;
                        case R.id.profile:
                            startActivity(new Intent(getContext(), ProfileActivity.class));
                            break;
                        case R.id.settings:
                            startActivity(new Intent(getContext(), SettingsActivity.class));
                            break;
                    }
                    return false;
                });
                menu.show();
                break;
            case R.id.baseCreateCompany:
                startActivity(new Intent(getContext(), CreateCompanyActivity.class));
                break;
            case R.id.baseCreateDoc:
                NewDocFragment.newInstance().show(getSupportFragmentManager(), "CreateNewDoc");
                break;
        }
    }
}
