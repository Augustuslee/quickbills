package com.kraftropic.quickbills.activities.creators;

import android.view.View;

import com.kraftropic.quickbills.R;
import com.kraftropic.quickbills.fragments.bottom_sheets.SignInPromptFragment;
import com.kraftropic.quickbills.models.Company;
import com.kraftropic.quickbills.quick_core.custom.QuickActivity;
import com.kraftropic.quickbills.utils.views.QuickEditText;

public class CreateCompanyActivity extends QuickActivity {

    QuickEditText name, alias, tagLine, email, phone, address, motto, vision, mission;

    @Override
    public int setLayout() {
        return R.layout.activity_create_company;
    }

    @Override
    public void quickResume() {
        setOnClickListenerById(R.id.createCompany);
        activateOnBackPressed();

        name = findViewById(R.id.companyName);
        alias = findViewById(R.id.companyAlias);
        tagLine = findViewById(R.id.companyTagLine);
        email = findViewById(R.id.companyEmail);
        phone = findViewById(R.id.companyPhone);
        address = findViewById(R.id.companyAddress);
        motto = findViewById(R.id.companyMotto);
        vision = findViewById(R.id.companyVision);
        mission = findViewById(R.id.companyMission);
    }

    @Override
    public void onClick(View v) {
        if (name.isNonNull()) {
            saveCompany(getCompany());
        }
    }

    private void saveCompany(Company company) {
        getApp().getRepo().insertCompany(company);
        if (getApp().getRepo().loggedIn()) {
            showAdThenQuit();
        } else {
            SignInPromptFragment.newInstance("Sign In?",
                    "With an account, your documents will always be backed up!").show(getSupportFragmentManager(), "SignInPrompt");
        }
    }

    private Company getCompany() {
        return new Company(
                name.getString(),
                alias.getString(),
                phone.getString(),
                email.getString(),
                address.getString(),
                tagLine.getString(),
                motto.getString(),
                vision.getString(),
                mission.getString(),
                R.layout.model_card_default,
                0
        );
    }
}
