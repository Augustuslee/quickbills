package com.kraftropic.quickbills.activities;

import com.kraftropic.quickbills.R;
import com.kraftropic.quickbills.quick_core.custom.QuickActivity;

public class ProfileActivity extends QuickActivity {

    @Override
    public int setLayout() {
        return R.layout.activity_profile;
    }

    @Override
    public void quickResume() {
        activateOnBackPressed();

    }
}
