package com.kraftropic.quickbills.activities;

import android.content.Intent;
import android.view.View;
import android.widget.Toast;

import com.kraftropic.quickbills.R;
import com.kraftropic.quickbills.fragments.bottom_sheets.RequestResetLinkFragment;
import com.kraftropic.quickbills.models.User;
import com.kraftropic.quickbills.quick_core.custom.QuickActivity;
import com.kraftropic.quickbills.utils.views.QuickEditText;

public class SignInActivity extends QuickActivity {

    QuickEditText email, password;

    @Override
    public int setLayout() {
        return R.layout.activity_create_sign_in;
    }

    @Override
    public void quickResume() {
        setOnClickListenerById(R.id.signIn);
        setOnClickListenerById(R.id.signUp);
        setOnClickListenerById(R.id.signInLater);
        setOnClickListenerById(R.id.signInGoogle);
        setOnClickListenerById(R.id.signInForgotPassword);

        email = findViewById(R.id.signInEmail);
        password = findViewById(R.id.signInPassword);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.signIn:
                if (email.isNonNull() && password.isNonNull()) {
                    getApp().getRepo().signInWithEmailAndPassword(email.getString(), password.getString(), (positive, s) -> {
                        Toast.makeText(SignInActivity.this, positive ? "Success, Welcome" : "Credentials don't match, Try Again", Toast.LENGTH_SHORT).show();
                        if (positive)
                            startAndFinishActivity(new Intent(getContext(), BaseActivity.class));
                    });
                }
                break;
            case R.id.signUp:
                startAndFinishActivity(new Intent(getContext(), SignUpActivity.class));
                break;
            case R.id.signInLater:
                startAndFinishActivity(new Intent(getContext(), BaseActivity.class));
                break;
            case R.id.signInGoogle:
                signUpWithGoogle();
                break;
            case R.id.signInForgotPassword:
                RequestResetLinkFragment.newInstance().show(getSupportFragmentManager(), "ResetPassword");
                break;
        }
    }

    @Override
    public void onSuccessfulGoogleSignIn(User user) {
        startAndFinishActivity(new Intent(getContext(), BaseActivity.class));
    }
}
