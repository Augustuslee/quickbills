package com.kraftropic.quickbills.activities.viewers;

import com.kraftropic.quickbills.R;
import com.kraftropic.quickbills.quick_core.custom.QuickActivity;

public class ViewBusinessCardActivity extends QuickActivity {

    @Override
    public int setLayout() {
        return R.layout.activity_view_business_card;
    }

    @Override
    public void quickResume() {
        activateOnBackPressed();

    }
}
