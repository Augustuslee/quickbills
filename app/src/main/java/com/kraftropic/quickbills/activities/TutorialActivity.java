package com.kraftropic.quickbills.activities;

import android.content.Intent;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.kraftropic.quickbills.R;
import com.kraftropic.quickbills.fragments.TutorialFragment;
import com.kraftropic.quickbills.quick_core.custom.QuickActivity;

import java.util.ArrayList;
import java.util.List;

import static com.kraftropic.quickbills.utils.Quick.tutorial;

public class TutorialActivity extends QuickActivity {

    private ViewPager viewPager;

    @Override
    public int setLayout() {
        return R.layout.activity_create_tutorial;
    }

    @Override
    public void quickResume() {
        viewPager = findViewById(R.id.tutorialViewPager);
        viewPager.setAdapter(new FragmentStatePagerAdapter(getSupportFragmentManager(), FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
            @NonNull
            @Override
            public Fragment getItem(int position) {
                return getTutorials().get(position);
            }

            @Override
            public int getCount() {
                return getTutorials().size();
            }
        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                ((Button) findViewById(R.id.tutorialNext)).setText(position == (getTutorials().size() - 1) ? "Start" : "Next");
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        setOnClickListenerById(R.id.tutorialNext);
        setOnClickListenerById(R.id.tutorialSkip);
    }

    public List<Fragment> getTutorials() {
        List<Fragment> fragments = new ArrayList<>();

        fragments.add(TutorialFragment.getInstance("Create Business Documents",
                "Create Invoices, Receipts, Letterheads, etc easily.", R.drawable.bg_create));
        fragments.add(TutorialFragment.getInstance("Share and send them on the go!",
                "Send, review documents on the go, you don't need an office", R.drawable.bg_phone));
        fragments.add(TutorialFragment.getInstance("Back them up on the cloud",
                "Create once and access your documents forever", R.drawable.bg_cloud));
        fragments.add(TutorialFragment.getInstance("Access them from anywhere",
                "Access your contacts, documents from different devices", R.drawable.bg_cross_platform));

        return fragments;
    }

    @Override
    public void onClick(View v) {
        if ((v.getId() == R.id.tutorialNext && viewPager.getCurrentItem() == (getTutorials().size() - 1)) || v.getId() == R.id.tutorialSkip) {
            getApp().getSharedPreferences().edit().putBoolean(tutorial, true).apply();
            startAndFinishActivity(new Intent(getContext(), SignInActivity.class));
        } else
            viewPager.setCurrentItem(viewPager.getCurrentItem() + 1, true);
    }
}
