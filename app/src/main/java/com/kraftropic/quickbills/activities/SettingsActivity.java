package com.kraftropic.quickbills.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.view.View;
import android.widget.Switch;
import android.widget.TextView;

import com.kraftropic.quickbills.R;
import com.kraftropic.quickbills.quick_core.custom.QuickActivity;

import static com.kraftropic.quickbills.utils.Quick.autoBackUp;

public class SettingsActivity extends QuickActivity {

    @Override
    public int setLayout() {
        return R.layout.activity_settings;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void quickResume() {
        activateOnBackPressed();

        setOnClickListenerById(R.id.settingsProfile);
        setOnClickListenerById(R.id.settingsKraftropic);
        setOnClickListenerById(R.id.settingsAbout);
        setOnClickListenerById(R.id.signOut);

        ((Switch) findViewById(R.id.backUpSwitch))
                .setOnCheckedChangeListener((buttonView, isChecked) -> {
                    getApp().getSharedPreferences().edit().putBoolean(autoBackUp, isChecked).apply();
                    buttonView.setChecked(getApp().getSharedPreferences().contains(autoBackUp) && getApp().getSharedPreferences().getBoolean(autoBackUp, true));
                });

        try {
            ((TextView) findViewById(R.id.settingsVersion)).setText("Version " + getPackageManager().getPackageInfo(getPackageName(), 0).versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.settingsProfile:
                startActivity(new Intent(getContext(), ProfileActivity.class));
                break;
            case R.id.settingsAbout:
                startActivity(new Intent(getContext(), AboutActivity.class));
                break;
            case R.id.settingsKraftropic:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(getResources().getString(R.string.kraftropic))));
                break;
            case R.id.signOut:
                signOut();
                startAndFinishByClass(SignInActivity.class);
                break;
        }
    }
}
