package com.kraftropic.quickbills.activities.creators;

import com.kraftropic.quickbills.R;
import com.kraftropic.quickbills.quick_core.custom.QuickActivity;

public class CreateContactActivity extends QuickActivity {

    @Override
    public int setLayout() {
        return R.layout.activity_create_contact;
    }

    @Override
    public void quickResume() {
        activateOnBackPressed();

    }
}
