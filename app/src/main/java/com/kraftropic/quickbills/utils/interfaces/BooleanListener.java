package com.kraftropic.quickbills.utils.interfaces;

public interface BooleanListener {
    void onBooleanEvent(boolean positive);
}
