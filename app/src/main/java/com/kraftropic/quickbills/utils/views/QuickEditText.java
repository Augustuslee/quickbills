package com.kraftropic.quickbills.utils.views;

import android.content.Context;
import android.util.AttributeSet;

import com.vanniktech.emoji.EmojiEditText;

public class QuickEditText extends EmojiEditText {
    public QuickEditText(Context context) {
        super(context);
    }

    public QuickEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public boolean isNonNull() {
        return getString().length() > 0;
    }

    public boolean passwordsMatch(String otherPassword) {
        if (!getString().equals(otherPassword)) {
            this.setError("Passwords do not match");
        }
        return getString().equals(otherPassword);
    }

    public String getString() {
        if (getText() != null)
            return this.getText().toString();
        else
            return "";
    }

    public int getInteger() {
        try {
            return Integer.parseInt(getString());
        } catch (Exception e) {
            return 0;
        }
    }

    public boolean isAnEmail() {
        if (isNonNull() && android.util.Patterns.EMAIL_ADDRESS.matcher(getString()).matches())
            return true;
        else {
            this.setError("Invalid email");
            return false;
        }
    }
}
