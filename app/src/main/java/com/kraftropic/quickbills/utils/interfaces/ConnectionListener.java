package com.kraftropic.quickbills.utils.interfaces;

public interface ConnectionListener {
    void onConnectionEvent(boolean connected);
}
