package com.kraftropic.quickbills.utils.interfaces;

public interface ObjectListener<T> {
    void onObjectEvent(boolean positive, T t);
}
