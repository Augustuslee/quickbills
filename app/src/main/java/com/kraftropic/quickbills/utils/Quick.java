package com.kraftropic.quickbills.utils;

public interface Quick {
    String pack = "com.kraftropic.quickbills";
    String signIn = "quickbills.sign_in";
    String signUp = "quickbills.sign_up";
    String tutorial = "quickbills.tutorial";
    String title = "quickbills.title";
    String summary = "quickbills.summary";
    String drawable = "quickbills.drawable";
    String backUp = "quickbills.backup";
    String uid = "quickbills.uid";
    String autoBackUp = "quickbills.auto_back_up";

    //firebase api
    String invoices = "quickbills.invoices";
    String receipts = "quickbills.receipts";
    String users = "quickbills.users";
    String companies = "quickbills.companies";
    String contacts = "quickbills.contacts";
    String letterheads = "quickbills.letterheads";
    String businessCards = "quickbills.businessCards";

    //storage
    String docs = "quickbills.docs";
    String images = "quickbills.images";
}
