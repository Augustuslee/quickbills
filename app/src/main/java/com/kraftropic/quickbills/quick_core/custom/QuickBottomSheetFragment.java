package com.kraftropic.quickbills.quick_core.custom;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.kraftropic.quickbills.quick_core.QuickApp;
import com.kraftropic.quickbills.utils.interfaces.BooleanListener;
import com.kraftropic.quickbills.utils.interfaces.ConnectionListener;

public abstract class QuickBottomSheetFragment extends BottomSheetDialogFragment implements View.OnClickListener, BooleanListener, ConnectionListener {

    View parent;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        parent = inflater.inflate(setLayout(), container, false);
        return parent;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.getViewTreeObserver().addOnGlobalLayoutListener(() -> {
            BottomSheetDialog dialog = (BottomSheetDialog) getDialog();
            assert dialog != null;
            FrameLayout bottomSheet = dialog.findViewById(com.google.android.material.R.id.design_bottom_sheet);
            assert bottomSheet != null;
            BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
            behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            behavior.setPeekHeight(0);
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        quickResume();
    }

    public void quickStart() {

    }

    public QuickActivity getQuickActivity() {
        return (QuickActivity) getActivity();
    }

    public <T extends View> T findViewById(int id) {
        return parent.findViewById(id);
    }

    public void setOnClickListenerById(int id) {
        findViewById(id).setOnClickListener(this);
    }

    public void setTextById(int id, String text) {
        ((TextView) findViewById(id)).setText(text);
    }

    public QuickApp getApp() {
        return getQuickActivity().getApp();
    }

    public abstract int setLayout();

    public abstract void quickResume();

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onBooleanEvent(boolean positive) {

    }

    public void startActivityAndFinish(Intent intent) {
        startActivity(intent);
        getQuickActivity().finish();
    }

    public void startActivityAndDismiss(Intent intent) {
        startActivity(intent);
        this.dismiss();
    }

    @Override
    public void onConnectionEvent(boolean connected) {

    }

    public String getSharedString(String id) {
        return getApp().getSharedPreferences().getString(id, "");
    }
}
