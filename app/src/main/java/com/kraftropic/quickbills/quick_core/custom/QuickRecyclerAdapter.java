package com.kraftropic.quickbills.quick_core.custom;

import android.content.Context;
import android.view.View;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public abstract class QuickRecyclerAdapter<T, H extends QuickViewHolder> extends RecyclerView.Adapter<H> implements Filterable {

    private List<T> ts;
    private View emptyView;

    public QuickRecyclerAdapter(List<T> ts, View emptyView) {
        this.ts = ts;
        this.emptyView = emptyView;
        setEmptyView();
    }

    private void setEmptyView() {
        emptyView.setVisibility(getItemCount() == 0 ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onBindViewHolder(@NonNull H holder, int position) {
        setEmptyView();
        onQuickBindViewHolder(holder, ts.get(position));
    }

    public abstract void onQuickBindViewHolder(H h, T t);

    @Override
    public int getItemCount() {
        return ts.size();
    }

    public Context getContext() {
        return emptyView.getContext();
    }
}
