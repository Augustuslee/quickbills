package com.kraftropic.quickbills.quick_core;

import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.firestore.FirebaseFirestore;
import com.kraftropic.quickbills.models.BusinessCard;
import com.kraftropic.quickbills.models.Company;
import com.kraftropic.quickbills.models.Contact;
import com.kraftropic.quickbills.models.Document;
import com.kraftropic.quickbills.models.Invoice;
import com.kraftropic.quickbills.models.LetterHead;
import com.kraftropic.quickbills.models.Receipt;
import com.kraftropic.quickbills.models.User;
import com.kraftropic.quickbills.quick_core.network.Fire;
import com.kraftropic.quickbills.quick_core.room.QuickDao;
import com.kraftropic.quickbills.quick_core.room.QuickDatabase;
import com.kraftropic.quickbills.utils.interfaces.ObjectListener;

import java.util.List;

import io.reactivex.Flowable;

import static com.kraftropic.quickbills.utils.Quick.autoBackUp;
import static com.kraftropic.quickbills.utils.Quick.businessCards;
import static com.kraftropic.quickbills.utils.Quick.companies;
import static com.kraftropic.quickbills.utils.Quick.contacts;
import static com.kraftropic.quickbills.utils.Quick.invoices;
import static com.kraftropic.quickbills.utils.Quick.letterheads;
import static com.kraftropic.quickbills.utils.Quick.receipts;
import static com.kraftropic.quickbills.utils.Quick.users;

public class QuickRepo {

    private static QuickRepo quickRepo;
    private QuickDao dao;
    private SharedPreferences preferences;

    static QuickRepo getInstance(QuickApp app, SharedPreferences preferences) {
        if (quickRepo == null)
            quickRepo = new QuickRepo(app, preferences);

        return quickRepo;
    }

    public boolean loggedIn() {
        return Fire.tool().signedIn();
    }

    public boolean autoBackUp() {
        return preferences.contains(autoBackUp) && preferences.getBoolean(autoBackUp, true);
    }

    public FirebaseFirestore getFirestore() {
        return FirebaseFirestore.getInstance();
    }

    QuickRepo(QuickApp application, SharedPreferences preferences) {
        this.preferences = preferences;
        QuickDatabase database = QuickDatabase.getDatabase(application);
        dao = database.wordDao();
    }

    public void insertUser(User word) {
        QuickDatabase.databaseWriteExecutor.execute(() -> dao.insertUser(word));
        if (loggedIn() && autoBackUp())
            uploadObjects(word, word.getUid(), users);
    }

    public void insertReceipt(Receipt receipt) {
        QuickDatabase.databaseWriteExecutor.execute(() -> dao.insertReceipt(receipt));
        if (loggedIn() && autoBackUp())
            uploadObjects(receipt, receipt.getUid(), receipts);
    }

    public void insertLetterHead(LetterHead letterHead) {
        QuickDatabase.databaseWriteExecutor.execute(() -> dao.insertLetterHead(letterHead));
        if (loggedIn() && autoBackUp())
            uploadObjects(letterHead, letterHead.getUid(), letterheads);
    }

    public void insertInvoice(Invoice invoice) {
        QuickDatabase.databaseWriteExecutor.execute(() -> dao.insertInvoice(invoice));
        if (loggedIn() && autoBackUp())
            uploadObjects(invoice, invoice.getUid(), invoices);
    }

    public void insertContact(Contact contact) {
        QuickDatabase.databaseWriteExecutor.execute(() -> dao.insertContact(contact));
        if (loggedIn() && autoBackUp())
            uploadObjects(contact, contact.getUid(), contacts);
    }

    public void insertCompany(Company company) {
        QuickDatabase.databaseWriteExecutor.execute(() -> dao.insertCompany(company));
        if (loggedIn() && autoBackUp())
            uploadObjects(company, company.getUid(), companies);
    }

    public void insertBusinessCard(BusinessCard businessCard) {
        QuickDatabase.databaseWriteExecutor.execute(() -> dao.insertBusinessCard(businessCard));
        if (loggedIn() && autoBackUp())
            uploadObjects(businessCard, businessCard.getUid(), businessCards);
    }

    public Flowable<List<User>> getUsers() {
        return dao.getUsers();
    }

    public Flowable<List<Receipt>> getReceipts() {
        return dao.getReceipts();
    }

    public Flowable<List<LetterHead>> getLetterHeads() {
        return dao.getLetterHeads();
    }

    public Flowable<List<Invoice>> getInvoices() {
        return dao.getInvoices();
    }

    public Flowable<List<Document>> getDocuments() {
        return dao.getDocuments();
    }

    public Flowable<List<Contact>> getContacts() {
        return dao.getContacts();
    }

    public Flowable<List<Company>> getCompanies() {
        return dao.getCompanies();
    }

    public Flowable<List<BusinessCard>> getBusinessCards() {
        return dao.getBusinessCards();
    }

    public void signInWithEmailAndPassword(String email, String password, ObjectListener<String> listener) {
        Fire.tool().auth().signInWithEmailAndPassword(email, password)
                .addOnSuccessListener(authResult -> {
                    if (listener != null)
                        listener.onObjectEvent(true, "Signed in successfully");
                })
                .addOnFailureListener(e -> {
                    if (listener != null)
                        listener.onObjectEvent(false, "Signed in Failed");
                });
    }

    public void requestPasswordResetLink(String email, ObjectListener<String> listener) {
        Fire.tool().auth().sendPasswordResetEmail(email)
                .addOnSuccessListener(authResult -> {
                    if (listener != null)
                        listener.onObjectEvent(true, "Reset email sent successfully");
                })
                .addOnFailureListener(e -> {
                    if (listener != null)
                        listener.onObjectEvent(false, "Reset email Failed");
                });
    }

    public void signUpWithEmailAndPassword(String email, String password, ObjectListener<String> listener) {
        Fire.tool().auth().createUserWithEmailAndPassword(email, password)
                .addOnSuccessListener(authResult -> {
                    if (listener != null)
                        listener.onObjectEvent(true, "Signed up successfully");
                })
                .addOnFailureListener(e -> {
                    if (listener != null)
                        listener.onObjectEvent(false, "Sign up Failed");
                });
    }

    public void uploadObjects(Object t, String uid, String table) {
        getFirestore().collection(table).document(uid)
                .set(t)
                .addOnCompleteListener(task -> Log.d(table, uid + " was successfully addded to backend"))
                .addOnFailureListener(e -> Log.e(table, uid + " failed to upload to backend", e));
    }

    public void uploadObjects(Object t, String uid, String table, ObjectListener<String> listener) {
        getFirestore().collection(table).document(uid)
                .set(t)
                .addOnSuccessListener(authResult -> {
                    if (listener != null)
                        listener.onObjectEvent(true, "Signed up successfully");
                })
                .addOnFailureListener(e -> {
                    if (listener != null)
                        listener.onObjectEvent(false, "Sign up Failed");
                });
    }
}
