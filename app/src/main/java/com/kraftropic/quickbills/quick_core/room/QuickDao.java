package com.kraftropic.quickbills.quick_core.room;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.kraftropic.quickbills.models.BusinessCard;
import com.kraftropic.quickbills.models.Company;
import com.kraftropic.quickbills.models.Contact;
import com.kraftropic.quickbills.models.Document;
import com.kraftropic.quickbills.models.Invoice;
import com.kraftropic.quickbills.models.LetterHead;
import com.kraftropic.quickbills.models.Receipt;
import com.kraftropic.quickbills.models.User;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public interface QuickDao {

    @Query("SELECT * FROM users order by date_modified ASC")
    Flowable<List<User>> getUsers();

    @Query("SELECT * FROM documents order by date_modified ASC")
    Flowable<List<Document>> getDocuments();

    @Query("SELECT * FROM companies order by date_modified ASC")
    Flowable<List<Company>> getCompanies();

    @Query("SELECT * FROM business_cards order by date_modified ASC")
    Flowable<List<BusinessCard>> getBusinessCards();

    @Query("SELECT * FROM contacts order by date_modified ASC")
    Flowable<List<Contact>> getContacts();

    @Query("SELECT * FROM receipts order by date_modified ASC")
    Flowable<List<Receipt>> getReceipts();

    @Query("SELECT * FROM invoices order by date_modified ASC")
    Flowable<List<Invoice>> getInvoices();

    @Query("SELECT * FROM letter_heads order by date_modified ASC")
    Flowable<List<LetterHead>> getLetterHeads();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertDocument(Document document);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertUser(User user);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertCompany(Company company);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertBusinessCard(BusinessCard businessCard);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertContact(Contact contact);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertReceipt(Receipt receipt);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertLetterHead(LetterHead letterHead);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertInvoice(Invoice invoice);

    @Query("DELETE FROM documents WHERE uid LIKE :docId")
    void deleteDocumentById(String docId);

    @Query("DELETE FROM users WHERE uid LIKE :userId")
    void deleteUser(String userId);
}
