package com.kraftropic.quickbills.quick_core.custom;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class QuickViewHolder extends RecyclerView.ViewHolder {

    public View parent;

    public QuickViewHolder(@NonNull View itemView) {
        super(itemView);
        parent = itemView;
    }

    public <T extends View> T findViewById(int id) {
        return parent.findViewById(id);
    }
}
