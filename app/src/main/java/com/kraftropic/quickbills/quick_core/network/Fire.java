package com.kraftropic.quickbills.quick_core.network;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;

public class Fire {

    private static FirebaseAuth auth = FirebaseAuth.getInstance();
    private static Fire fire;

    public static Fire tool() {
        if (fire == null)
            fire = new Fire();
        return fire;
    }

    public boolean signedIn() {
        return auth.getCurrentUser() != null;
    }

    public FirebaseAuth auth() {
        return auth;
    }

    public static FirebaseFirestore firestore() {
        return FirebaseFirestore.getInstance();
    }

    public static FirebaseStorage storage() {
        return FirebaseStorage.getInstance();
    }
}
