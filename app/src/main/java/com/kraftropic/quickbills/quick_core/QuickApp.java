package com.kraftropic.quickbills.quick_core;

import android.app.Application;
import android.content.SharedPreferences;

import com.google.android.gms.ads.MobileAds;
import com.google.firebase.FirebaseApp;
import com.kraftropic.quickbills.R;
import com.vanniktech.emoji.EmojiManager;
import com.vanniktech.emoji.ios.IosEmojiProvider;

import static com.kraftropic.quickbills.utils.Quick.pack;

public class QuickApp extends Application {

    private SharedPreferences sharedPreferences;
    private QuickRepo repo;

    @Override
    public void onCreate() {
        super.onCreate();
        initApp();
        EmojiManager.install(new IosEmojiProvider());
    }

    public void initApp() {
        sharedPreferences = getSharedPreferences(pack, MODE_PRIVATE);
        repo = QuickRepo.getInstance(this, getSharedPreferences());
        FirebaseApp.initializeApp(this);
        MobileAds.initialize(this, getResources().getString(R.string.admob_id));
    }

    public SharedPreferences getSharedPreferences() {
        return sharedPreferences;
    }

    public QuickRepo getRepo() {
        return repo;
    }
}
