package com.kraftropic.quickbills.quick_core.room;


import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.kraftropic.quickbills.models.BusinessCard;
import com.kraftropic.quickbills.models.Company;
import com.kraftropic.quickbills.models.Contact;
import com.kraftropic.quickbills.models.Document;
import com.kraftropic.quickbills.models.Invoice;
import com.kraftropic.quickbills.models.LetterHead;
import com.kraftropic.quickbills.models.Receipt;
import com.kraftropic.quickbills.models.User;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {User.class, BusinessCard.class, Document.class, Company.class, Contact.class, Invoice.class, LetterHead.class, Receipt.class}, version = 3, exportSchema = false)
@TypeConverters({TimeMachine.class})
public abstract class QuickDatabase extends RoomDatabase {

    public abstract QuickDao wordDao();

    private static volatile QuickDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 4;
    public static final ExecutorService databaseWriteExecutor = Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    public static QuickDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (QuickDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            QuickDatabase.class, "quick_database")
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return INSTANCE;
    }
}
