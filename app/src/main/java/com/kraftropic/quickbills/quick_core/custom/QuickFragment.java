package com.kraftropic.quickbills.quick_core.custom;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.kraftropic.quickbills.quick_core.QuickApp;
import com.kraftropic.quickbills.utils.interfaces.BooleanListener;
import com.kraftropic.quickbills.utils.interfaces.ConnectionListener;

public abstract class QuickFragment extends Fragment implements View.OnClickListener, BooleanListener, ConnectionListener {

    View parent;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        parent = inflater.inflate(setLayout(), container, false);
        return parent;
    }

    @Override
    public void onResume() {
        super.onResume();
        quickResume();
    }

    public void quickStart() {

    }

    public QuickActivity getQuickActivity() {
        return (QuickActivity) getActivity();
    }

    public <T extends View> T findViewById(int id) {
        return parent.findViewById(id);
    }

    public void setOnClickListenerById(int id) {
        findViewById(id).setOnClickListener(this);
    }

    public QuickApp getApp() {
        return getQuickActivity().getApp();
    }

    public abstract int setLayout();

    public abstract void quickResume();

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onBooleanEvent(boolean positive) {

    }

    @Override
    public void onConnectionEvent(boolean connected) {

    }

    public String getSharedString(String id) {
        return getApp().getSharedPreferences().getString(id, "");
    }
}
