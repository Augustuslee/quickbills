package com.kraftropic.quickbills.quick_core.custom;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;
import com.kraftropic.quickbills.R;
import com.kraftropic.quickbills.models.User;
import com.kraftropic.quickbills.quick_core.QuickApp;
import com.kraftropic.quickbills.utils.interfaces.BooleanListener;
import com.kraftropic.quickbills.utils.interfaces.ConnectionListener;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

public abstract class QuickActivity extends AppCompatActivity implements View.OnClickListener, BooleanListener, ConnectionListener {

    private Uri selectedUri;
    private InterstitialAd interstitialAd;
    private AdView adView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(setLayout());

        try {
            Intent appLinkIntent = getIntent();
            String appLinkAction = appLinkIntent.getAction();
            Uri appLinkData = appLinkIntent.getData();
        } catch (Exception ignored) {
        }
        quickStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        quickResume();
    }

    public void quickStart() {

    }

    public void onBackPress(View view) {
        onBackPressed();
    }

    public void activateOnBackPressed() {
        findViewById(R.id.back).setOnClickListener(v -> onBackPressed());
    }

    public void setOnClickListenerById(int id) {
        findViewById(id).setOnClickListener(this);
    }

    public QuickApp getApp() {
        return (QuickApp) getApplication();
    }

    public Context getContext() {
        return this;
    }

    public abstract int setLayout();

    public abstract void quickResume();

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onBooleanEvent(boolean positive) {

    }

    @Override
    public void onConnectionEvent(boolean connected) {

    }

    public void startAndFinishActivity(Intent intent) {
        startActivity(intent);
        finish();
    }

    public String getSharedString(String id) {
        return getApp().getSharedPreferences().getString(id, "");
    }

    public void showAdThenQuit() {
        adView = findViewById(R.id.adview);
        adView.setVisibility(View.VISIBLE);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        adView.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                onBackPressed();
                adView.setVisibility(View.GONE);
            }

            @Override
            public void onAdLeftApplication() {
                onBackPressed();
                adView.setVisibility(View.GONE);
            }
        });
    }

    public void startCam() {
        if (ContextCompat.checkSelfPermission(Objects.requireNonNull(this), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(Objects.requireNonNull(this), new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
        } else {
            requestCamera();
        }
    }

    public void startFile() {
        Intent pickIntent = new Intent();
        pickIntent.setType("image/*");
        pickIntent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(pickIntent, 200);
    }

    private void requestCamera() {
        Intent pictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (pictureIntent.resolveActivity(Objects.requireNonNull(this).getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                Toast.makeText(this, "Something went wrong opening the camera", Toast.LENGTH_SHORT).show();
            }
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(Objects.requireNonNull(this), "com.kraftropic.quickbills.core.provider", photoFile);
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(pictureIntent, 100);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 100) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                requestCamera();
            } else {
                Toast.makeText(this, "We need your permission to access the camera, click to allow", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 200 || requestCode == 100) {
                CropImage.activity(
                        requestCode == 100 ? selectedUri : data.getData())//camera, or  file
                        .start(this);
            } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                selectedUri = CropImage.getActivityResult(data).getUri();
                onFileSelected(selectedUri);
            } else if (requestCode == 300) {
                if (data.getData() != null)
                    onFileSelected(data.getData());
                else Toast.makeText(this, "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
            Toast.makeText(this, "Something went wrong, try again", Toast.LENGTH_SHORT).show();
        } else if (requestCode == 21) {
            try {
                firebaseAuthWithGoogle(Objects.requireNonNull((GoogleSignIn.getSignedInAccountFromIntent(data)).getResult(ApiException.class)));
            } catch (ApiException e) {
                Toast.makeText(getContext(), "Something went wrong, please try again", Toast.LENGTH_SHORT).show();
                Log.w("SignUpWithGoogle", "Google sign in failed", e);
            }
        }
    }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir = Objects.requireNonNull(this).getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, ".jpg", storageDir);

        selectedUri = Uri.fromFile(image);
        return image;
    }

    public void onFileSelected(Uri fileUri) {

    }

    public User getGoogleUser(GoogleSignInAccount account) {
        return new User(
                FirebaseAuth.getInstance().getUid(),
                account.getDisplayName(),
                "",
                account.getEmail(),
                Calendar.getInstance().getTime(),
                ""
        );
    }

    public void signUpWithGoogle() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        GoogleSignInClient client = GoogleSignIn.getClient(this, gso);
        Intent signInIntent = client.getSignInIntent();
        startActivityForResult(signInIntent, 21);
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        FirebaseAuth.getInstance().signInWithCredential(credential)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        onSuccessfulGoogleSignIn(getGoogleUser(acct));
                    } else {
                        Toast.makeText(getContext(), "Something went wrong, please try again", Toast.LENGTH_SHORT).show();
                    }
                });
    }


    public void onSuccessfulGoogleSignIn(User user) {

    }

    public void signOut() {
        FirebaseAuth.getInstance().signOut();
    }

    public void startAndFinishByClass(Class<?> klass) {
        startAndFinishActivity(new Intent(this, klass));
    }
}
